# Introducción

¡Welcome to ASALTO PIRATA! a game where the player must use his intellect in order to answer the fearsome pirates.

## Instrucciones del juego

"Asalto Pirata" is an interactive game and therefore requieres constant interacion between player and the pirate enemy. The game is based on an exchange of funny insults/answers.
The first turn is randomply chosen ath the beggining of the game. The player must win three rounds in order to beat his oponent. 

The main game scene will be briefly detailed below:

- On the upper sides there are the player (left side) and the enemy (right side) punctuations.
- On the upper side we can see a piece of manuscript where the attacks/defenses of the oponent will be appearing.
- On the bottom side there is another piece of manuscript where we can see which ansers or attacks the user can choose from.
- You can click at any time the ESC button in order to pause the actual game.

## Dev information

This game was my first approach to Unity engine and its 2D possibilities.
At this moment the game is not translated to english. This translation will be add in the future.



