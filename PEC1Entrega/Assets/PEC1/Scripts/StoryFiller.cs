﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StoryFiller : MonoBehaviour {
    /// <summary>
    /// Creates a lis with all the possible attack-defense
    /// </summary>
    /// <returns>List of answers</returns>
    public static List<GameManagerScript.TextToSpeak> FillInsultList()
    {
        List<GameManagerScript.TextToSpeak> answers = new List<GameManagerScript.TextToSpeak>();

        GameManagerScript.TextToSpeak node1 = CreateInsult("¡Llevarás mi espada como si fueras un pincho moruno!",
            "Primero deberías dejar de usarla como un plumero.");
        answers.Add(node1);
        GameManagerScript.TextToSpeak node2 = CreateInsult("¡Luchas como un granjero!", "Qué apropiado, tú peleas como una vaca.");
        answers.Add(node2);
        GameManagerScript.TextToSpeak node3 = CreateInsult("¡No hay palabras para describir lo asqueroso que eres!", "Sí que las hay, sólo que nunca las has aprendido.");
        answers.Add(node3);
        GameManagerScript.TextToSpeak node4 = CreateInsult("¡No pienso aguantar tu insolencia aquí sentado! ", "Ya te están fastidiando otra vez las almorranas, ¿Eh?");
        answers.Add(node4);
        GameManagerScript.TextToSpeak node5 = CreateInsult("¡Mi pañuelo limpiará tu sangre!", "Ah, ¿Ya has obtenido ese trabajo de barrendero?");
        answers.Add(node5);
        GameManagerScript.TextToSpeak node6 = CreateInsult("¡Ha llegado tu HORA, palurdo de ocho patas!", "Y yo tengo un SALUDO para ti, ¿Te enteras?");
        answers.Add(node6);
        GameManagerScript.TextToSpeak node7 = CreateInsult("¿Has dejado ya de usar pañales?", "¿Por qué? ¿Acaso querías pedir uno prestado?");
        answers.Add(node7);
        GameManagerScript.TextToSpeak node8 = CreateInsult("¡Una vez tuve un perro más listo que tu!", "Te habrá enseñado todo lo que sabes.");
        answers.Add(node8);
        GameManagerScript.TextToSpeak node9 = CreateInsult("¡Nadie me ha sacado sangre jamás, y nadie lo hará!", "¿TAN rápido corres?");
        answers.Add(node9);
        GameManagerScript.TextToSpeak node10 = CreateInsult("¡Me das ganas de vomitar! ", "Me haces pensar que alguien ya lo ha hecho.");
        answers.Add(node10);
        GameManagerScript.TextToSpeak node11 = CreateInsult("¡Tienes los modales de un mendigo!", "Quería asegurarme de que estuvieras a gusto conmigo.");
        answers.Add(node11);
        GameManagerScript.TextToSpeak node12 = CreateInsult("¡He oído que eres un soplón despreciable!", "Qué pena me da que nadie haya oído hablar de ti.");
        answers.Add(node12);
        GameManagerScript.TextToSpeak node13 = CreateInsult("¡La gente cae a mis pies al verme llegar!", "¿Incluso antes de que huelan tu aliento?");
        answers.Add(node13);
        GameManagerScript.TextToSpeak node14 = CreateInsult("¡Demasiado bobo para mi nivel de inteligencia!", "Estaría acabado si la usases alguna vez.");
        answers.Add(node14);
        GameManagerScript.TextToSpeak node15 = CreateInsult("¡Obtuve esta cicatriz en mi cara en una lucha a muerte!", "Espero que ya hayas aprendido a no tocarte la nariz.");
        answers.Add(node15);
        GameManagerScript.TextToSpeak node16 = CreateInsult("¡He hablado con simios más educados que tu!", "Me alegra que asistieras a tu reunión familiar diaria.");
        answers.Add(node16);

        return answers;
    }
    /// <summary>
    /// Creates a TextToSpeak node with an insult and with its valid answer
    /// </summary>
    /// <param name="insult"></param>
    /// <param name="answer"></param>
    /// <returns></returns>
    public static GameManagerScript.TextToSpeak CreateInsult(string insult, string answer)
    {
        GameManagerScript.TextToSpeak node = new GameManagerScript.TextToSpeak();
        node.insult = insult;
        node.answer = answer;
        return node;
    }
}
