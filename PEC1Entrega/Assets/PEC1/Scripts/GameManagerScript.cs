﻿using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManagerScript : MonoBehaviour
{

    public class TextToSpeak
    {
        ///<<summary>
        ///Insult that can be say by the player
        ///</summary>
        public string insult;
        /// <summary>
        /// Text used to contrarest the insult
        /// </summary>
        public string answer;
    }

    public Text iaPunctuation;
    public Text playerPunctuation;
    public Text iaText;
    public GameObject button;
    public Transform buttonListContent;
    public GameObject buttonListStack;
    public GameObject scrollBar;

    private List<TextToSpeak> textList = new List<TextToSpeak>();
    public static int victoryIACount;
    public static int victoryPlayerCount;
    private int player;
    private bool endGame;

    // Use this for initialization
    void Start()
    {
        playerPunctuation.text = "0";
        iaPunctuation.text = "0";
        iaText.text = "";
        victoryIACount = 0;
        victoryPlayerCount = 0;
        textList = StoryFiller.FillInsultList();
        DestroyUserOptions();
        player = TurnSelector();
        TurnAction();
    }

    /// <summary>
    /// Redirects the game flow depending of the player/IA turn
    /// </summary>
    void TurnAction()
    {
        switch(player)
        {
            case 0:
                FillUI("attack");
            break;

            case 1:
                SelectRandomAttack();
                FillUI("defense");
            break;
        }
    }
    
    /// <summary>
    /// Selects a random attack to show to the player
    /// </summary>
    async void SelectRandomAttack()
    {
        buttonListStack.SetActive(false);
        var index = Random.Range(0,16);
        var node = textList[index];
        await WriteText(node.insult);
        buttonListStack.SetActive(true);
    }

    /// <summary>
    /// Creates an initial list of random answers and adds the correct one in order to make the decision 
    /// possibilities more close to the valid one.
    /// </summary>
    /// <param name="playerAttack"></param>
    /// <returns>Random answer to defense against the player attack</returns>
    TextToSpeak SelectRandomNodeAnswer(string playerAttack)
    {
        //Get the node with the original insult in order to have the correct answer
        var correctNode = SearchOriginalNode(playerAttack);
        List<TextToSpeak> possibleNodeAnswersList = new List<TextToSpeak>();
        possibleNodeAnswersList.Add(correctNode);
        for(int i=0; i<4; i++)
        {
            var index = Random.Range(0, 16);
            var node = textList[index];
            if(node != correctNode || !possibleNodeAnswersList.Contains(node))
            {
                possibleNodeAnswersList.Add(node);
            }
            else
            {
                //Si el nodo ya existia se debera decrementar i para mantener las 5 posibles respuestas de la ia
                if (i > 0)
                {
                    i--;
                }
            }
        }
        var answerIndex = Random.Range(0, 5);
        var answer = possibleNodeAnswersList[answerIndex];
        return answer;
    }

    /// <summary>
    /// Destroy all the buttons created on the user UI (list of attack/defense)
    /// </summary>
    void DestroyUserOptions()
    {
        foreach (Transform child in buttonListContent.transform)
        {
            Destroy(child.gameObject);
        }
    }

    /// <summary>
    /// Fills the player UI with attacks/defense
    /// </summary>
    /// <param name="turnType">Decide if the list will be of attack or defense</param>
    void FillUI(string turnType)
    {
        //Destroy all elements
        DestroyUserOptions();
        //Instantiate all elements on te user UI from the list given
        foreach(TextToSpeak text in textList)
        {
            GameObject buttonAnswerCopy = Instantiate(button);
            buttonAnswerCopy.transform.SetParent(buttonListContent, false);
            if (turnType =="attack")
            {
                buttonAnswerCopy.GetComponentInChildren<Text>().text = text.insult;
                buttonAnswerCopy.GetComponentInChildren<Text>().color = Color.black;
                FillListener(buttonAnswerCopy.GetComponent<Button>(), turnType);
            }
            else
            {
                buttonAnswerCopy.GetComponentInChildren<Text>().text = text.answer;
                buttonAnswerCopy.GetComponentInChildren<Text>().color = Color.black;
                FillListener(buttonAnswerCopy.GetComponent<Button>(),turnType);
            }
        }
        //Sets scroll zone to the initial position (top)
        buttonListStack.GetComponent<ScrollRect>().verticalNormalizedPosition = 1;
    }

    /// <summary>
    /// Add to buttons the action thath will be executed on click
    /// </summary>
    /// <param name="button"></param>
    /// <param name="turnType"></param>
    void FillListener(Button button, string turnType)
    {
        button.onClick.AddListener(() =>
        {
            TextSelected(turnType, button);
        });
    }

    /// <summary>
    /// Decide which will be the next action of the user/IA depending of the params
    /// </summary>
    /// <param name="turnType"></param>
    /// <param name="button"></param>
    void TextSelected(string turnType, Button button)
    {
        if(turnType == "attack")
        {
            IaDefense(button);
        }
        else
        {
            PlayerDefense(button);
        }
    }

    /// <summary>
    /// Answers the player attack and checks if wins or loses the round
    /// </summary>
    /// <param name="button"></param>
    async void IaDefense(Button button)
    {
        buttonListStack.SetActive(false);
        var playerInsult = button.GetComponentInChildren<Text>().text;
        var nodeAnswer = SelectRandomNodeAnswer(playerInsult);
        await WriteText(nodeAnswer.answer);
        buttonListStack.SetActive(true);
        if (nodeAnswer.insult == playerInsult)
        {
            await Task.Delay(1000);
            //Ronda ganada por la IA
            victoryIACount++;
            iaPunctuation.text = victoryIACount.ToString();
            player = 1;
            CheckPunctuation();
            TurnAction();
        }
        else
        {
            victoryPlayerCount++;
            playerPunctuation.text = victoryPlayerCount.ToString();
            player = 0;
            CheckPunctuation();
            TurnAction();
        }
    }

    /// <summary>
    /// Answers the IA attack and checks if wins or loses the round
    /// </summary>
    /// <param name="button"></param>
    void PlayerDefense(Button button)
    {
        //Defense sobre iaText
        var buttonText = button.GetComponentInChildren<Text>().text;
        var node = SearchOriginalNode(iaText.text);
        if (node.answer == buttonText)
        {
            //Player wins
            victoryPlayerCount++;
            playerPunctuation.text = victoryPlayerCount.ToString();
            player = 0;
            iaText.text = "";
            CheckPunctuation();
            if(endGame == false)
            {
                TurnAction();
            }   
        }
        else
        {
            //Ia wins
            victoryIACount++;
            iaPunctuation.text = victoryIACount.ToString();
            player = 1;
            CheckPunctuation();
            if(endGame == false)
            {
                TurnAction();
            }
        }
    }

    /// <summary>
    /// Search the node relative to the param 
    /// </summary>
    /// <param name="text"></param>
    /// <returns></returns>
    TextToSpeak SearchOriginalNode(string text)
    {
        TextToSpeak result = null;
        foreach(var node in textList)
        {
            if (node.insult == text)
            {
                result = node;
                break;
            }          
        }
        return result;
    }

    /// <summary>
    /// Determinates if IA or player will start playing
    /// </summary>
    /// <returns></returns>
    int TurnSelector()
    {
        var player = Random.Range(0,2); 
        return player;
    }

    /// <summary>
    /// Function that will check if the user/IA has arrived to the 3 points to win
    /// </summary>
    void CheckPunctuation()
    {
        if (victoryPlayerCount == 3 || victoryIACount == 3)
        {
            endGame = true;

            SceneManager.LoadScene("EndGameScene");
        }
    }

    /// <summary>
    /// Shows in screen with 50ms delay the ia answer/attack
    /// </summary>
    /// <param name="textToWrite"></param>
    /// <returns></returns>
    async Task WriteText(string textToWrite)
    {
        if(textToWrite != null)
        {
            string currentText = "";
            for (int i = 0; i <= textToWrite.Length; i++)
            {
                currentText = textToWrite.Substring(0, i);
                iaText.text = currentText;
                await Task.Delay(50);
            }
        }
    }
}
