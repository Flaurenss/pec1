﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
/// <summary>
/// Control the color changes related to suer interaction 
/// </summary>
public class UserButtonController : MonoBehaviour {

    public GameObject button;

    public void OnPointerEnter()
    {
        button.GetComponentInChildren<Text>().color = Color.gray;
    }

    public void OnPointerExit()
    {
        button.GetComponentInChildren<Text>().color = Color.black;
    }
}
