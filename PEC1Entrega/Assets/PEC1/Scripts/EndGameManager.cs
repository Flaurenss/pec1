﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EndGameManager : MonoBehaviour {

    public AudioClip winSound;
    public AudioClip loseSound;
    public GameObject endGameText;
    public GameObject backgroundImage;

    private AudioSource soundToPlay;
	// Use this for initialization
	void Start () {
        soundToPlay = GetComponent<AudioSource>();
        CheckResult();
	}

    ///<<summary>
    ///Comprueba el ganador y se encarga de cargar la imagen de fondo correcta asi como el texto y 
    ///activar el sonido correspondiente.
    ///</summary>
    void CheckResult()
    {
        if(GameManagerScript.victoryPlayerCount == 3)
        {
            backgroundImage.GetComponent<Image>().sprite = Resources.Load<Sprite>("treasure_fix");
            endGameText.GetComponent<Text>().text = "VICTORIA";
            endGameText.GetComponent<Text>().color = Color.green;
            soundToPlay.clip = winSound;
            soundToPlay.Play();
        }
        else if(GameManagerScript.victoryIACount == 3)
        {
            backgroundImage.GetComponent<Image>().sprite = Resources.Load<Sprite>("pirate_flag");
            endGameText.GetComponent<Text>().text = "DERROTA";
            endGameText.GetComponent<Text>().color = Color.red;
            soundToPlay.clip = loseSound;
            soundToPlay.Play();
        }
    }
}
